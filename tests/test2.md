# Markdown teszt 1

## "Egységtesztek"

### Bekezdések

Szöveg szöveg szöveg
ez nem új bekezdés ám!

Szöveg szöveg szöveg

Szöveg szöveg szöveg

### Betűstílusok

Szöveg _szöveg_ szöveg

_Szöveg szöveg_ szöveg

Szöveg **szöveg szöveg**

### Kódrészletek

    def fun():
	    pass

    def fun2():
	    fun()

A szövegben is szerepelhet kód, például egy `lambda x: x&y` kifejezés, vagy néhány `<html></html>` tag.

### Idézetek

> A politikus olyan, mint a pelenka:
> rendszeresen le kell cserélni,
> és ugyanabból az okból.
>
> Mark Twain

### Linkek

[Gugli](http://www.google.com) a barátod. De próbálkozhatsz az [Internet Tudakozóval](http://www.internettudakozo.hu) is.

### Számozatlan listák

- Nem
- számozott
- lista

* Másik
* nem
* számozott

- Kevert
+ szintaxis
* is lehet

### Számozott listák

1. kettő
2. három
3. négy

## Egymásba ágyazott listák

1. egy
  - másfél
  - kétharmad
2. kettő
  1. huszonegy
  2. negyvenkettő

- nem
  1. de
  2. mégsem
- sem
  - hova?
  - szalámit

## Minden egyben

**Félkövér** szöveg _dőlt_ szöveg `<code>_ez nem lehet dőlt_</code>` kód [link](http://www.example.com) link.

Még egy idézet:

> Some people, when confronted with a problem,
> think “I know, I’ll use regular expressions.”
> Now they have two problems.

Még egy kód:

    _ez sem lehet dőlt_
		

