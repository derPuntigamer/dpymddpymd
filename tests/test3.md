# Markdown teszt 1

## "Egységtesztek"

### Bekezdések

Szöveg szöveg szöveg
ez nem új bekezdés ám!

Szöveg szöveg szöveg

Szöveg szöveg szöveg

### _Betű_ **stílusok**

Szöveg _szöveg_ szöveg **szöveg**

_Szöveg **szöveg**_ szöveg

Szöveg **szöveg _szöveg_**

### Kódrészletek `_inline_`

    def fun(args):
	    pass

    def fun2():
	    fun('_code_')

A szövegben is szerepelhet kód, például egy `lambda x: x&y` kifejezés, vagy néhány `<html></html>` tag.

### Idézetek

> A politikus olyan, mint a pelenka:
> rendszeresen le kell cserélni,
> és **ugyanabból az okból**.
>
> _Mark **Twain**_
>
> `marktwain="'"`

### Linkek

[**Gugli**](http://www.google.com) a barátod. De próbálkozhatsz az [_Internet Tudakozóval_](http://www.internettudakozo.hu) is.

A link szövege lehet [`<code>`](http://www.example.com) is.

### Számozatlan listák

- Nem
- számozott
- lista

* Másik
* nem
* számozott

- Kevert
+ szintaxis
* is lehet

### Számozott listák

1. kettő
2. három
3. négy

## Egymásba ágyazott listák

1. egy
  - másfél
  - kétharmad
2. kettő
  1. huszonegy
  2. negyvenkettő

- nem
  1. _de_
  2. **mégsem**
- sem
  - hova?
  - szalámit
  - `#include <potz.h>`

## Egyéb

**Félkövér** szöveg _dőlt_ szöveg `<code>_ez nem lehet dőlt_</code>` kód [link](http://www.example.com) link.

Még egy idézet:

> Some people, when confronted with a problem,
> think “I know, I’ll use regular expressions.”
> Now they have two problems.

Még egy kód:

    _ez sem lehet dőlt_


