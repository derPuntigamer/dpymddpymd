#!/usr/bin/python3

"""
    ***** dpymd *****
    Markdown 2 HTML converter
    Author: Daniel Dudola
"""

import sys
import re

import mdc_libs.elements as elements


# check if all input is given
if len(sys.argv) != 3:
    print("Usage: ./mdconvert.py <INPUT.md> <OUTPUT.html>")
    sys.exit(1)

# open Markdown file specified in input
try:
    mdfile = open(sys.argv[1])
except FileNotFoundError:
    print("Input file " + sys.argv[1] + " was not found")
    sys.exit(1)


# list for storing classes containing processed data
# it is no stack, the name only shows its function
my_stack     = []

# global variables storing file processing states
isBlockQuote = False
isParahraph  = False
isList       = False
isCode       = False

# all lines are rstrip-ped and stored in this list for processing
lines = list(map(str.rstrip, mdfile.readlines()))

# processing the file line by line
for line_num, line in enumerate(lines):

    #### Setting states
    # check if line is in <blockquote>
    if not line.startswith(">"):
        isBlockQuote = False

    if not line.startswith("    ") and not line.startswith("\t") and line:
        isCode = False

    # check if <p> ended
    if not line or line.startswith(">"):
        isParahraph = False

    # check if list ended
    if (not re.match("[-|\+|\*] ", line)     and
        not re.match("\d.\ ", line)          and
        not re.match("\s+ [-|\+|\*] ", line) and
        not re.match("\s+ \d.\ ", line)):
        isList = False

    # skip nested list item lines
    if re.match("\s+ [-|\+|\*] ", line) or re.match("\s+ \d.\ ", line):
        isList = True
        continue

    #### Skipping lines which were processed elsewhere
    # skip paragraph and list item lines
    if isParahraph or isList or isCode:
        continue

    #### Process special elements into objects
    # find header lines
    if line.startswith("#"):
        level   = str(len(line.split()[0]))
        content = " ".join(line.split()[1:])
        my_stack.append(elements.Header(content, level))

    # find blockquotes
    elif line.startswith(">"):
        if not isBlockQuote:
            start_line = line_num
            my_code = elements.BlockQuote()
            isBlockQuote = True
            plus_index = 0

            while True:
                if lines[start_line + plus_index].startswith(">"):
                    my_code.addline(lines[start_line + plus_index][1:])
                    plus_index += 1
                else:
                    my_stack.append(my_code)
                    break

    # find lists
    elif re.match("-|\+|\* ", line) or re.match("\d.\ ", line):
        first_line = line_num
        my_list = elements.List()
        isList = True
        plus_index = 0

        while True:
            try:
                my_line = lines[first_line + plus_index]

                if (re.match("-|\+|\*",     my_line) or
                    re.match("\d.\ ",       my_line) or
                    re.match("\s+ -|\+|\*", my_line) or
                    re.match("\s+ \d.\ ",   my_line)
                    ):

                    my_list.addline(lines[first_line + plus_index])
                    plus_index += 1
                else:
                    my_stack.append(my_list)
                    break
            except IndexError:
                my_stack.append(my_list)
                break

    # find codeblocks
    elif not line and not isCode:
        empty_line = line_num
        my_code = elements.CodeBlock()
        plus_index = 1

        while True:
            try:
                spaced = lines[empty_line + plus_index].startswith("    ")
                tabbed = lines[empty_line + plus_index].startswith("\t")

                if spaced or tabbed:
                    my_code.addline(lines[empty_line + plus_index][4:])
                    plus_index += 1
                elif not lines[empty_line + plus_index]:
                    my_code.addline("")
                    plus_index += 1
                else:
                    if not my_code.code_lines:
                        del my_code
                        isCode = False
                        break
                    else:
                        isCode = True
                        my_stack.append(my_code)
                    break
            except IndexError:
                isCode = True
                my_stack.append(my_code)
                break

    #### everything else goes into a paragraph
    else:
        first_line = line_num
        myParagraph = elements.Paragraph()
        isParahraph = True
        plus_index = 0

        while True:
            try:
                if lines[first_line + plus_index]:
                    myParagraph.add_content(
                        lines[first_line + plus_index] + " ")
                    plus_index += 1
                else:
                    my_stack.append(myParagraph)
                    break
            except IndexError:
                break


#### processing stored objects into output file
output_file = open(sys.argv[2], 'w')

print(elements.HTML_Boilerplate.start(), file = output_file)

for elem in my_stack:
    print(elem.convert(), file = output_file)

print(elements.HTML_Boilerplate.end(), file = output_file)

output_file.close()
sys.exit(0)