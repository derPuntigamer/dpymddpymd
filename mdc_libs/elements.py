from . import methods
import re

"""
    Object library for mdconvert:
    load up objects from __main__ and use them to get HTML output
    calling their convert() function
"""


class HTML_Boilerplate:
    """
        class to add HTML HTML_Boilerplate
        I added UTF-8 support and
        a stylesheet too from GitHub

    """

    @staticmethod
    def start():
        return (
            "<!DOCTYPE html>\n<html><head>\n"
            "    <title>Markdown output</title>\n"
            "    <meta charset=\"UTF-8\" />\n"
            "    <link rel=\"stylesheet\" href=\"stylesheets/github.css\"/>"
            "</head>\n<body>\n\n"
        )

    @staticmethod
    def end():
        return (
            "\n\n</body></html>"
        )


class Paragraph:
    """object to store and convert paragraph content"""

    def __init__(self):
        self.my_content = ""

    def add_content(self, added_content):
        self.my_content += added_content

    def convert(self):
        self.my_content = methods.format(self.my_content)
        return (
            "<p>" + self.my_content + "</p>"
        )


class Header:
    """object to store and convert header elements"""

    def __init__(self, my_content, my_level):
        self.my_content = my_content
        self.my_level   = my_level

    def convert(self):
        self.my_content = methods.format(self.my_content)
        return (
            "<h" + self.my_level + ">" +
            self.my_content + "</h" + self.my_level + ">"
        )


class ListStorage:
    """list storage parent class, do not instancize"""

    def __init__(self):
        self.code_lines = []

    def addline(self, my_line):
        self.code_lines.append(my_line)


class CodeBlock(ListStorage):
    """object to store and convert codeblock content"""

    def __init__(self):
        super().__init__()

    def convert(self):
        # removing blank lines from the end of the list
        while not self.code_lines[-1]:
            del self.code_lines[-1]

        return_string = "<pre><code>"
        for line in self.code_lines:
            line = methods.format_code(line)
            return_string += line + "\n"
        return_string += "</code></pre>"
        return return_string


class BlockQuote(ListStorage):
    """object to store and convert blockquote content"""

    def __init__(self):
        super().__init__()

    def convert(self):
        return_string = "<blockquote>\n"
        for line in self.code_lines:
            line = methods.format(line)
            # if line is empty, append a line break tag
            if line:
                return_string += "  " + line + "\n"
            else:
                return_string += "  <br><br>\n"
        return_string += "</blockquote>"
        return return_string


class List(ListStorage):
    """object to store and convert both kind of lists"""

    def __init__(self):
        super().__init__()

    def preprocess(self):
        # processes input into dictonaries containing content, indentation and
        # type information, use as input for the convert() function
        preprocessed = []

        for line in self.code_lines:

            if re.match("[-\+\*]", line) or re.match("\s* [-\+\*]", line):
                list_type = "unordered"
            else:
                list_type = "ordered"

            indent  = (len(line) - len(line.lstrip())) / 2
            content = " ".join(line.split()[1:])
            content = methods.format(content)
            preprocessed.append({
                                "content" : content,
                                "indent"  : int(indent),
                                "type"    : list_type
                                })

        return preprocessed

    def convert(self):
        line_list     = self.preprocess()
        indent_stack  = []
        prev_indent   = 0
        return_string = ""

        # begin with an opening tag
        if line_list[0]["type"] == "unordered":
            return_string += "<ul>\n"
            indent_stack.append("unordered")
        else:
            return_string += "<ol>\n"
            indent_stack.append("ordered")

        for line in line_list:
            # get inner in indentation levels
            if prev_indent < line["indent"]:
                if line["type"] == "unordered":
                    return_string += "<ul>\n"
                    indent_stack.append("unordered")
                else:
                    return_string += "<ol>\n"
                    indent_stack.append("ordered")

            # get outter in indentation levels
            while prev_indent > line["indent"]:
                type_was = indent_stack.pop()

                if type_was == "unordered":
                    return_string += "</ul>\n"
                else:
                    return_string += "</ol>\n"

                prev_indent -= 1

            # add list item content
            return_string += "  <li>" + line["content"] + "</li>\n"
            prev_indent = line["indent"]

        # add closing tags until reaching indentation level 0
        while prev_indent > 0:
                type_was = indent_stack.pop()

                if type_was == "unordered":
                    return_string += "</ul>\n"
                else:
                    return_string += "</ol>\n"

                prev_indent -= 1

        # close list with closing tag
        type_was = indent_stack.pop()

        if type_was == "unordered":
            return_string += "</ul>\n\n"
        else:
            return_string += "</ol>\n\n"

        return return_string

