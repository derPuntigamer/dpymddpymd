import re

"""
    Function library for mdconvert:
    apply string formatting functions defined here on contents while loading up
    and/or processing HTML objects defined in elements.py module
"""


def find_links(line):
    """find and process links"""
    while re.search("\[(.*?)\]\((.*?)\)", line):
        search_match = re.search("\[(.*?)\]\((.*?)\)", line).span()

        eleje = line[:search_match[0]]
        link  = line[search_match[0]:search_match[1]+1]
        vege  = line[search_match[1]:]

        text  = link[link.find("[")+1:link.find("]")]
        url   = link[link.find("(")+1:link.find(")")]

        html_link = "<a href=\"" + url + "\">" + text + "</a>"
        html_line = eleje + html_link + vege

        line = html_line
    return line


def format_styles(line):
    """find and process bold and italic tags"""
    words   = line.split()
    pre     = []
    my_line = []

    for word in words:
        # check word beginnings
        if word.startswith('_**'):
            my_word = list(word)
            my_word.pop(0)
            my_word.pop(0)
            my_word[0] = "<em><strong>"
            pre.append("".join(my_word))

        elif word.startswith('**_'):
            my_word = list(word)
            my_word.pop(0)
            my_word.pop(0)
            my_word[0] = "<strong><em>"
            pre.append("".join(my_word))

        elif word.startswith("**"):
            my_word = list(word)
            my_word[0] = "<strong>"
            del my_word[1]
            pre.append("".join(my_word))

        elif word.startswith("_"):
            my_word = list(word)
            my_word[0] = "<em>"
            pre.append("".join(my_word))

        elif re.search("\W\*\*\w", word):
            my_word = list(word)
            tag_index = re.search("\W\*\*\w", word).start() + 1
            my_word.pop(tag_index)
            my_word[tag_index] = "<strong>"
            pre.append("".join(my_word))

        elif re.search("\W_\w", word):
            my_word = list(word)
            tag_index = re.search("\W_\w", word).start() + 1
            my_word[tag_index] = "<em>"
            pre.append("".join(my_word))

        else:
            pre.append(word)

    for word in pre:
        # check word endings
        if word.endswith('_**'):
            my_word = list(word)
            my_word.pop(-1)
            my_word.pop(-1)
            my_word[-1] = "</em></strong>"
            my_line.append("".join(my_word))

        elif word.endswith('**_'):
            my_word = list(word)
            my_word.pop(-1)
            my_word.pop(-1)
            my_word[-1] = "</strong></em>"
            my_line.append("".join(my_word))

        elif word.endswith("**"):
            my_word = list(word)
            my_word.pop(-1)
            my_word[-1] = "</strong>"
            my_line.append("".join(my_word))

        elif word.endswith("_"):
            my_word = list(word)
            my_word[-1] = "</em>"
            my_line.append("".join(my_word))

        elif re.search("\w\*\*\W", word):
            my_line.append("</strong>".join(word.split("**")))

        elif re.search("\w_\W", word):
            my_line.append("</em>".join(word.split("_")))

        else:
            my_line.append(word)

    return " ".join(my_line)


def find_inline_code(line):
    """find and process inline code snippets"""
    # content will be returned in a list with notice on formatting
    # True -> formatting enabled, False -> Formatting disabled
    my_content = []
    # get ` character indexes from string if any
    borders = [i for i, ltr in enumerate(line) if ltr == '`']

    # process lines not starting with an inline code block
    if borders and len(borders) % 2 == 0 and 0 not in borders:

        my_content.append([line[0:borders[0]], True])

        for i in range(len(borders)):
            if i % 2 != 0:
                try:
                    my_content.append([line[borders[i] + 1:borders[i + 1]], True])
                except IndexError:
                    my_content.append([line[borders[i] + 1:], True])
            else:
                try:
                    code = "<code>" + line[borders[i] + 1:borders[i + 1]] + "</code>"
                    my_content.append([code, False])
                except IndexError:
                    code = "<code>" + line[borders[i] + 1:] + "</code>"
                    my_content.append([code, False])

        return my_content

    # process lines starting with an inline code block
    elif borders and len(borders) % 2 == 0 and 0 in borders:

        for i in range(len(borders)):
            if i % 2 != 0:
                try:
                    my_content.append([line[borders[i] + 1:borders[i + 1]], True])
                except IndexError:
                    my_content.append([line[borders[i] + 1:], True])
            else:
                try:
                    code = "<code>" + line[borders[i] + 1:borders[i + 1]] + "</code>"
                    my_content.append([code, False])
                except IndexError:
                    code = "<code>" + line[borders[i] + 1:] + "</code>"
                    my_content.append([code, False])

        return my_content

    # if no inline codeblock found, return the line with formatting enabled
    else:
        return [[line, True]]


def format(line):
    """apply all formatting steps in one function"""
    # replace special characters
    line = line.replace("&", "&amp;")
    line = line.replace("<", "&lt;")
    line = line.replace(">", "&gt;")

    # search for inline code snippets (returns a list!)
    tags = find_inline_code(line)

    # apply style formatting
    edited_line = []

    for tag in tags:
        if tag[1]:
            edited_line.append(format_styles(tag[0]))
        else:
            edited_line.append(tag[0])

    line = " ".join(edited_line)

    # search for links, contents are already formatted
    line = find_links(line)

    return line


def format_code(line):
    """reduced formatting function for code snippets"""
    line = line.replace("&", "&amp;")
    line = line.replace("<", "&lt;")
    line = line.replace(">", "&gt;")
    return line